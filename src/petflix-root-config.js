import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@petflix/decide",
  app: () => System.import("@petflix/decide"),
  activeWhen: ["/films"],
});

// registerApplication(
//   "@petflix/inspire",
//   () => System.import("@petflix/inspire"),
//   (location) => {
//     return (
//       location.pathname !== "/films" && location.pathname.startsWith("/films")
//     );
//   }
// );

registerApplication({
  name: "@petflix/inspire",
  app: () => System.import("@petflix/inspire"),
  activeWhen: ["/inspire"]
});

registerApplication({
  name: "@petflix/team-marcopolo",
  app: () => System.import("@petflix/team-marcopolo"),
  activeWhen: ["/"]
});

// registerApplication({
//   name: "@single-spa/welcome",
//   app: () =>
//     System.import(
//       "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
//     ),
//   activeWhen: ["/"],
// });

// registerApplication({
//   name: "@petflix/navbar",
//   app: () => System.import("@petflix/navbar"),
//   activeWhen: ["/"]
// });

start({
  urlRerouteOnly: true,
});
