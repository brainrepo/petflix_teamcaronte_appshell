# Team caronte responsabile dell'ingress

## Installazione dell'app

    npx create-single-spa

## Avvio dell'app

    npm start

Se ci dovessero essere problemi di porta andarla a sostituire nel package.json e rilanciare

    npm start

Aggiornare anche su index.ejs la porta del root config

    <% if (isLocal) { %>
        <script type="systemjs-importmap">
            {
            "imports": {
                "@petflix/root-config": "//localhost:9191/petflix-root-config.js" 
            }
            }
        </script>
    <% } %>

# Collego l'app del team decide

nell'index.ejs devo inserire il link per caricare il modulo del team decide

    <% if (isLocal) { %>
    <script type="systemjs-importmap">
        {
            "imports": {
            "@petflix/root-config": "//localhost:9191/petflix-root-config.js",
            "@petflix/decide": "//localhost:92/petflix-decide.js"
            }
        }
    </script>
    <% } %>

Il team decide usa react quindi nella pagina dovrò metterci anche le deps relative a react

    <script type="systemjs-importmap">
        {
            "imports": {
            "single-spa": "https://cdn.jsdelivr.net/npm/single-spa@5.5.5/lib/system/single-spa.min.js",
            "react": "https://cdn.jsdelivr.net/npm/react@16.13.1/umd/react.production.min.js",
            "react-dom": "https://cdn.jsdelivr.net/npm/react-dom@16.13.1/umd/react-dom.production.min.js"
            }
        }
    </script>



Inserisco su petflix-root-config.js

    registerApplication({
    name: "@petflix/decide",
    app: () => System.import("@petflix/decide"),
    activeWhen: ["/films"]
    });


# Disattivo l'app di default

commento da petflix-root-config.js

    // registerApplication({
    //   name: "@single-spa/welcome",
    //   app: () =>
    //     System.import(
    //       "https://unpkg.com/single-spa-welcome/dist/single-spa-welcome.js"
    //     ),
    //   activeWhen: ["/"],
    // });

# Attivo l'app del team inspire

Inserisco la dep nell'index.ejs

    "@petflix/inspire": "//localhost:93/main.js"

Registro l'applicazione nel petflix-root-config.js

    registerApplication({
    name: "@petflix/inspire",
    app: () => System.import("@petflix/inspire"),
    activeWhen: ["/inspire"]
    });

Decommento l'import di zone.js da index.ejs

    <!-- <script src="https://cdn.jsdelivr.net/npm/zone.js@0.10.3/dist/zone.min.js"></script> -->

Da questo momento in poi tutto funziona